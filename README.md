# Marcus test task


# Setup

Install docker & docker-compose:
* docker 19+
* docker-compose 1.23+

# Run

Build containers and run application:
- `docker-compose up --build`

Frontend is running on:
http://localhost:4200/

Backend is running on:
http://localhost:5000/
