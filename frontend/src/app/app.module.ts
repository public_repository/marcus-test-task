import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbToastModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewPersonModal } from './new-person-modal/new-person-modal.component';
import { ToastsContainer } from './toast/toast-container.component';

import { Service } from '../service/service';
import { ToastService } from './toast/toast-service';


@NgModule({
  declarations: [
    AppComponent,
    NewPersonModal,
    ToastsContainer,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbToastModule,
  ],
  providers: [Service, ToastService],
  bootstrap: [AppComponent],
  entryComponents: [ NewPersonModal ]
})
export class AppModule { }
