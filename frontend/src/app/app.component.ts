import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { NewPersonModal } from './new-person-modal/new-person-modal.component';

import { Service } from '../service/service';

export interface Person {
  name: string;
  phone: string;
  address: string;
}

export interface SearchQuery {
  searchString: string,
  searchField: string,
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  people: Person[] = [];
  searchForm: FormGroup;

  constructor(private modalService: NgbModal, private service: Service) {
    this.service.getPeople().subscribe(people => this.people = people);
    this.searchForm = new FormGroup({
      searchField: new FormControl('name'),
      searchString: new FormControl(''),
    });
  }

  openNewPersonModal() {
    const modalRef = this.modalService.open(NewPersonModal).result.then(
      () => {
        this.search();
      },
      () => {}
    );
  }

  search() {
    const value = this.searchForm.value;
    this.service.searchPeople(value.searchField, value.searchString).subscribe(people => this.people = people);
  }
}
