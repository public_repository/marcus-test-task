import { Component, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl } from '@angular/forms';

import { ToastService } from '../toast/toast-service';
import { Service } from '../../service/service';
import { Person } from '../app.component';

const TOAST_DELAY: number = 5000;

@Component({
  selector: 'app-new-person-modal',
  templateUrl: './new-person-modal.component.html',
})
export class NewPersonModal {
  personForm: FormGroup;

  constructor(public activeModal: NgbActiveModal, private service: Service, public toastService: ToastService) {
    this.personForm = new FormGroup({
      name: new FormControl(null),
      phone: new FormControl(null),
      address: new FormControl(null),
    });
  }

  addPerson() {
    this.service.addPerson(this.personForm.value).subscribe(person => {
      this.showSuccess(person);
      this.activeModal.close();
    },
      e => {
        this.showDanger(e.error.message);
      })
  }

  showSuccess(person: Person) {
    this.toastService.show(
      `Person "${person.name}" created successfully`,
      { classname: 'bg-success text-light', delay: TOAST_DELAY }
    );
  }

  showDanger(message: string) {
    this.toastService.show(
      `The following error occured: "${message}"`,
      { classname: 'bg-danger text-light', delay: TOAST_DELAY }
    );
  }
}
