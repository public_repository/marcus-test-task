import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Person } from '../app/app.component';
import { Observable } from 'rxjs';


@Injectable()
export class Service {
  peopleUrl: string = environment.apiUrl;


  constructor(private http: HttpClient) { }

  getPeople(): Observable<Person[]> {
    return this.http.get<Person[]>(this.peopleUrl);
  }

  getPerson(id: number): Observable<Person> {
    return this.http.get<Person>(`${this.peopleUrl}/${id}`);
  }

  searchPeople(search_by: string, value: string): Observable<Person[]> {
    return this.http.get<Person[]>(`${this.peopleUrl}?search_by_${search_by}=${value}`);
  }

  addPerson(person: Person): Observable<Person> {
    return this.http.post<Person>(this.peopleUrl, person);
  }

  updatePerson(id: number, person: Person): Observable<Person> {
    return this.http.put<Person>(`${this.peopleUrl}/${id}`, person);
  }
}
