import argparse
import csv
import os
import sqlite3
from typing import Dict, List

FIELDS = ('id', 'name', 'phone', 'address')


def write_csv(file, fieldnames: List[str], data: List[Dict[str, str]]):
    writer = csv.DictWriter(file, fieldnames=fieldnames)
    writer.writeheader()
    for person in data:
        writer.writerow(person)


def read_sqlite(db_path: str, file) -> List[Dict[str, str]]:
    con: sqlite3.Connection = sqlite3.connect(db_path)
    with sqlite3.connect(db_path) as con:
        writer = csv.DictWriter(file, fieldnames=FIELDS)
        writer.writeheader()
        for line in con.cursor().execute(f'select {",".join(FIELDS)} from People'):
            person = {key: value for (key, value) in zip (FIELDS, line)}
            writer.writerow(person)


def create_dump_file(db_file, dump_file):
    folder = os.path.dirname(dump_file)
    if folder:
        os.makedirs(folder, exist_ok=True)
    with open(dump_file, 'w', newline='') as csvfile:
        read_sqlite(db_file, csvfile)


parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-i', '--db_file', type=str, default='db.sqlite3', help='path to database file')
parser.add_argument('-o', '--output_file', type=str, default='dump.csv', help='path and name of csv file for db dump')


if __name__ == '__main__':
    args = parser.parse_args()
    create_dump_file(args.db_file, args.output_file)
