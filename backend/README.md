# Express/Node.js API

# Setup 
1. Run `npm install` to install dependencies
2. Run server via `npm run start`

Server should be running on 127.0.0.1:5000

# Endpoints

GET:  
- /people - returns data about all people  
- /people/:id - returns data about single person by id  
- /people?search_by_name=:name - returns list of people that match the param.
Available query params are: `search_by_name`, `search_by_address`, `search_by_phone`.  

POST:  
- /people - create new person. 
`name`, `address` and `phone` is required form params.

PUT:
- /people/:id - edit person data by id.
`name`, `address` and `phone` is required form params.


# DB dump
To create a db dump file run `python db_dump.py`.
This will create a `dump.csv` file in `backend directory.

Optional arguments for creating dump file:
- -i, --db_file --- path to db.sqlite3 file
- -o, --output_file --- path to csv file

Example:  
- `python db_dump.py -i path/to/db.sqlite3 -o path/to/dump.csv`
