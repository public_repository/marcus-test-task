const path = require('path');
const corsMiddleware = require('restify-cors-middleware');
const Sequelize = require('sequelize');
const finale = require('finale-rest');
const restify = require('restify');

const SERVER_HOST = process.env.HOST || '0.0.0.0';
const SERVER_PORT = process.env.PORT || 5000;
const DB_LOCATION = process.env.DB_LOCATION || path.join(__dirname, 'db.sqlite3');

const SEARCH_PREFIX = 'search_by';

// Define your models
const database = new Sequelize({
  dialect: 'sqlite',
  storage: DB_LOCATION
});

const Person = database.define('Person', {
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  address: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  phone: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      isMobilePhone: true,
    }
  },
});

const server = restify.createServer()
const cors = corsMiddleware({
  preflightMaxAge: 5, // Optional
  origins: ['*'], // Should whitelist actual domains in production
  allowHeaders: ['Authorization', 'API-Token', 'Content-Range'], //Content-range has size info on lists
  exposeHeaders: ['Authorization', 'API-Token-Expiry', 'Content-Range']
})

server.pre(cors.preflight)
server.use(cors.actual)
server.use(restify.plugins.queryParser()); //{mapParams: true}
server.use(restify.plugins.bodyParser());  //{mapParams: true, mapFiles: true}
server.use(restify.plugins.acceptParser(server.acceptable));

// Initialize finale
finale.initialize({
  app: server,
  sequelize: database
});

// Create REST resource
const personResource = finale.resource({
  model: Person,
  endpoints: ['/people', '/people/:id'],
  search: [
    {
      operator: Sequelize.Op.like,
      param: `${SEARCH_PREFIX}_name`,
      attributes: ['name']
    },
    {
      operator: Sequelize.Op.like,
      param: `${SEARCH_PREFIX}_address`,
      attributes: ['address']
    },
    {
      operator: Sequelize.Op.like,
      param: `${SEARCH_PREFIX}_phone`,
      attributes: ['phone']
    }
  ]
});

personResource.list.fetch.before((req, res, context) => {
  const searchQueries = Object.keys(req.query).filter(
    (key) => key.includes(SEARCH_PREFIX)
  );
  if (searchQueries.length > 1) {
    throw new finale.Errors.BadRequestError(message='Only one search param are allowed!')
  }
  return context.continue;
})

// Create database and listen
database
  .sync()
  .then(function() {
    server.listen(SERVER_PORT, SERVER_HOST, function() {
      const host = server.address().address;
      const port = server.address().port;

      console.log('listening at http://%s:%s', host, port);
    });
  });
